#pragma once

using namespace std;

class Timer
{
public:
	Timer();

	boost::timer::cpu_timer _cpu_timer;

	chrono::time_point<chrono::steady_clock> _start_time_point;
	chrono::duration<double>                 _elapsed_duration;
}
