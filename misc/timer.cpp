#include <boost/timer/timer.hpp>
#include <chrono>
#include "timer.h"

using namespace std;

Timer::Timer(const bool& init_stopped=false)
{
	if (init_stopped)
		_cpu_timer.stop();
}

void Timer::start()
{
	_cpu_timer.start();

	_start_time_point = chrono::steady_clock::now();
	_elapsed_duration = chrono::steady_clock::now() - chrono::steady_clock::now();
}

void Timer::stop()
{
	_cpu_timer.stop();

	_elapsed_duration = chrono::steady_clock::now() - _start_time_point;
}

bool Timer::is_stopped()
{
	return _cpu_timer.is_stopped();
}

Timer::elapsed()
{
	auto elapsed = _cpu_timer.elapsed();

	chrono::duration<double> elapsed_duration = chrono::steady_clock::now() - _start_time_point;
}
