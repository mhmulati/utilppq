#include "util.h"
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <unistd.h>
using namespace std;

/*! put str back to the istream data */
void putback(istream& data, const string& str) {
	for (int i = str.length() - 1; i >= 0; --i)
		data.putback(str[i]);
}

bool is_stdin_empty() {
	bool is_stdin_empty_ = isatty(fileno(stdin));  //!< see https://social.msdn.microsoft.com/Forums/vstudio/en-US/a41811a2-e4ab-4398-b8d4-6749fae0755c/non-blocking-reading-from-stdin-?forum=vcgeneral
	if (not is_stdin_empty_) {
		string tok;
		cin >> tok;
		is_stdin_empty_ = tok.empty();
		if (not is_stdin_empty_) {
			putback(cin, tok);
			cin.putback(' ');
		}
	}
	return is_stdin_empty_;
}

void get_data_from_file(const string& fn, stringstream& data) {
	ifstream file(fn);
	fail_if_(not file.is_open(), "Error: get_data_from_file: could not read file " + fn);
	file.seekg(0, ios::end);
	streampos len = file.tellg();
	file.seekg(0, ios::beg);
	while (file.tellg() < len)
		data << file.rdbuf();
	data << flush;
	file.close();
}

void get_data_from_stdin_or_file(const string& fn, stringstream& data, const bool try_stdin) {
	bool is_stdin_empty_ = true;
	if (try_stdin)
		is_stdin_empty_ = is_stdin_empty();

	if (not fn.empty() and try_stdin and not is_stdin_empty_) {
		fail("Error: get_data_from_stdin_or_file: there are input filename and data in stdin (user must supply exactly one).");  //stdin:#" + s + "#");
	}
	else if (not fn.empty()) {
		get_data_from_file(fn, data);
	}
	else if (try_stdin and not is_stdin_empty_) {
		data << cin.rdbuf();
	}
	else if (try_stdin) {
		fail("Error: get_data_from_stdin_or_file: there is no input filename nor stdin data");
	}
	else {
		fail("Error: get_data_from_stdin_or_file: there is no input filename");
	}
}

void print_sequence_header() {
	cout << "[";
}

void print_sequence_footer() {
	cout << "]" << endl;
}

