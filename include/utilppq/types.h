#pragma once
#include <vector>
#include <iostream>
using namespace std;

template <typename T=double> using matrix = vector<vector<T>>;

template <typename T=double> using block = vector<matrix<T>>;

template <typename T=double> class Coords {
public:
	T x;
	T y;

	Coords(T x_, T y_) {
		x = x_;
		y = y_;
	}
	friend ostream& operator<<(ostream& os, const Coords& c) {
	    os << '[' << c.x << ',' << c.y << ']';
	    return os;
	}
};
