#pragma once

#ifndef DEFSELLOG
	#ifdef DEFLOG
		#include "actlog.h"
	#else
		#include "deactlog.h"
	#endif
#else
	#include "deactlog.h"
#endif

#ifdef DEFCHECK
	#define CHK(cmd) cmd
#else
	#define CHK(cmd)
#endif

#ifdef DEFDETAILCUTS
	#define DTC(cmd) cmd
#else
	#define DTC(cmd)
#endif

#ifdef DEFDETAILSOLS
	#define DTS(cmd) cmd
#else
	#define DTS(cmd)
#endif

/*
#ifdef DEFBBTREE
	#define BBT(cmd) cmd
#else
	#define BBT(cmd)
#endif
 */
