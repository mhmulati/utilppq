#pragma once

const double EPS = 0.000001;

const double PI  = 3.141592;
const double RRR = 6378.388;

const unsigned long  B = 1;
const unsigned long KB = 1024;
const unsigned long MB = 1024 * 1024;
const unsigned long GB = 1024 * 1024 * 1024;  // 1'073'741'824
