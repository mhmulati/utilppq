#ifdef LOG
	#undef LOG
#endif

#ifdef LGN
	#undef LGN
#endif

#define LOG(cmd) cmd
#define LGN(cmd) clog << cmd << endl
