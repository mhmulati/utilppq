/* header *///@{
#pragma once
#include "types.h"
#include "prepr.h"
#include "consts.h"
#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <limits>
#include <type_traits>
using namespace std;  //< FIXME: Put only the ones used in this file, not the entire namespace std
//@}

template <typename T>
inline const string bool_to_str(const T& v) {
	if (is_same<T, bool>::value)
		return v ? "true" : "false";
	return "";
}

template <typename T>
inline bool leq_one(const T& v, const T& eps) {
	return v <= 1 - eps;
}

template <typename T>
inline bool geq_zero(const T& v, const T& eps) {
	return v >= eps;
}

template <typename T>
inline bool is_zero(const T& v, const T& eps) {
	return abs(v) <= eps or v == -0;
}

template <typename T>
inline bool is_one(const T& v, const T& eps) {
	return abs(v - 1.0) <= eps;
}

template <typename T>
inline bool eq(const T& a, const T& b, const T& eps) {
	return abs(a - b) <= eps;
}

template <typename T>
inline string z_to_str(const T& z, const T& inf=numeric_limits<T>::max()) {
	if (z >=  inf) return  "inf";
	if (z <= -inf) return "-inf";
	return to_string(z);
}

template <typename T>
inline string x_to_str(const T& x, const T& eps) {
	if (   -eps <= x and x <=     eps) return "0";
	if (1 - eps <= x and x <= 1 + eps) return "1";
	return to_string(x);
}

template<typename T=vector<double>, typename U=vector<string>, typename V=int>
string fmtvars(const T& vals, const U& names, const V& b, const V& len) {
	stringstream ss;
	bool first = true;
	for (auto i = b; i < b + len; ++i)
		if (geq_zero(vals[i])) {
			if (not first)
				ss <<  ", ";
			else
				first = false;
			ss << names[i];
			if (leq_one(vals[i]))
				ss << ": " + to_string(vals[i]);
		}
	return ss.str();
}

/*! see https://www.thecodingforums.com/threads/how-do-you-create-and-use-an-ostringstream-in-an-initialisation-list.543728/ */
template<typename T> string from_stream_to_str(T x) {
	stringstream ss;
	ss << x;
	return ss.str();
}

inline float num_dec_places_round(const float& x, const int num_dec_places) {
   int pow_ = pow(10, num_dec_places);
   return round(x * pow_) / pow_;
}

/*! Nearest integer. To be used accordingly TSPLIB95 */
template<typename T=float>
inline int nint(const T& x) {
   return (int) (x + 0.5);
}

template<typename T=float>
inline int nearest_int(const T& x) {
   return (int) (x + 0.5);
}

/*! Nearest integer as float, double, ... To be used accordingly TSPLIB95. Adapted to use float return */
template<typename T=float>
inline T nintf(const T& x, const bool int_dist_measure=true, const int num_dec_places=0) {
	if (int_dist_measure)
		return (T) nint(x);
	else if (!num_dec_places)
		return x;
	else
		return num_dec_places_round(x, num_dec_places);
}

template<typename T=double>
inline bool is_int(const T& x, const T& eps) {
	T int_part;
	return eq(modf(x, &int_part), 0.0, eps);
}

inline double geocoord_to_rad(double geocoord) {
	int degrees = (int) geocoord;
	double minutes = geocoord - degrees;
	return PI * (degrees + 5.0 * minutes / 3.0) / 180.0;
}

/* TODO
inline string mv_zeros(T&)
template <typename T=const int>
{
	if ()
		return
}
*/

template <typename T=const string>
inline void fail(T msg="Error: fail: generic", string fn=__FILE__, int ln=__LINE__) {
	cerr << msg << " | fn: " << fn << " ln : " << ln << endl;
	exit(EXIT_FAILURE);
}

template <typename T=const string>
inline void fail_if_(const bool fail_, T msg="Error: fail_if_: generic", string fn=__FILE__, int ln=__LINE__) {
	if (fail_)
        fail(msg, fn, ln);
}

template <typename T=const string>
inline void success(T msg="Not an error: success: generic") {
	cerr << msg << endl;
	exit(EXIT_SUCCESS);
}

template <typename T=const string>
inline bool proceed_only_if_(const bool ok, T msg="Error: proceed_if_: generic", string fn=__FILE__, int ln=__LINE__) {
	if (not ok)
		fail(msg, fn, ln);
	return true;
}

template <typename T=const string&>
void turn_true_only_once_or_fail(bool& flag, T msg="Error: turn_true_only_once_or_fail: generic", string fn=__FILE__, int ln=__LINE__) {
	if (!flag)
		flag = true;
	else
        fail(msg, fn, ln);
}

template <typename T, typename S=const string, typename Ss =const vector<string>>
T to_code(S& key_str, Ss& key_strs) {
	for (unsigned int i = 0; i < key_strs.size(); i++)
		if (key_str == key_strs[i])
			return (T) i;
	return T::NONE;
}

template <typename T, typename S=const string, typename Ss=const vector<string>>
S & from_code(T key_code, Ss& key_strs) {
	return key_strs.at(static_cast<int>(key_code));
}

template <typename T=int>
string to_idx(const T& i, const string& pre="_", const string& pos="_") {
	return pre + to_string(i) + pos;
}

template <typename T=vector<float>>
string cntner_to_str(const T& cntner, const string& sep="\t", const string& pre="", const string& pos="", const string& first_pre="", const string& last_pos="", const bool& show_idx = false, const string& idx_pre="", const string& idx_pos="") {
	stringstream ss;
	string first_pre_ = !first_pre.empty() ? first_pre : pre;
	string last_pos_  = !last_pos.empty()  ? last_pos  : pos;
	int i = 0;
	for (auto it = cntner.begin(); it != cntner.end();)
	{
		if (it != cntner.begin() && show_idx)
			ss << to_idx(i, idx_pre, idx_pos) << pre;
		else
			ss << first_pre_;
		ss << *it++;
		if (it != cntner.end())
			ss << pos << sep;
		else
			ss << last_pos_;
		i++;
	}
	return ss.str();
}

template <typename T=matrix<float>, typename S=string>
S sqr_cntner_to_str(const T& sqr_cntner, const S& sep="\t", const S& brek="\n") {
	stringstream ss;
	if (!sqr_cntner.empty())
	{
		unsigned int k = 0;
		if (!sqr_cntner.front().empty())
			ss << sep << to_idx(k++);
		for (; k < sqr_cntner.front().size(); k++)
			ss << sep << to_idx(k);
		ss << brek;
	}
	int i = 0;
	for (auto& it : sqr_cntner)
	{
		ss << to_idx(i++) << sep;
		ss << cntner_to_str(it, sep);
		ss << brek;
	}
	return ss.str();
}

template <typename T=matrix<float>>
string dyn_sqr_cntner_to_str(const T& dyn_sqr_cntner, const string& sep="\t", const string& pre="", const string& pos="", const string& first_pre="", const string& last_pos="", const string& brek="\n") {
	stringstream ss;
	string first_pre_ = !first_pre.empty() ? first_pre : pre;
	string last_pos_  = !last_pos.empty()  ? last_pos  : pos;
	int i = 0;
	auto it = dyn_sqr_cntner.begin();
	while (it != dyn_sqr_cntner.end())
	{
		if (it != dyn_sqr_cntner.begin())
			ss << to_idx(i, "/* ", " */") << pre;
		else
			ss << first_pre_;
		ss << cntner_to_str(*it, sep);
		++it;
		if (it != dyn_sqr_cntner.end())
			ss << pos << brek;
		else
			ss << last_pos_;
		i++;
	}
	return ss.str();
}

template <typename Matrix=matrix<double>>
string matrix_to_str_(const Matrix& T, const string& name, const double& eps) {
	stringstream ss;
	if (not name.empty())
		ss << name << ": {";
	bool first = true;
	for (int i = 0; i < static_cast<int>(T.size()); ++i)
		for (int j = 0; j < static_cast<int>(T[i].size()); ++j)
			if (not is_zero(T[i][j], eps)) {
				if (not first)
					ss <<  ", ";
				else
					first = false;
				ss << "[" + to_string(i) + "," + to_string(j) + "]";
				if (not is_one(T[i][j], eps))
					ss << ": " + to_string(T[i][j]);
			}
	if (not name.empty())
		ss << "}";
	return ss.str();
}

// TODO. For implement Python's "in", check: https://stackoverflow.com/questions/44622964/what-is-the-c-equivalent-of-pythons-in-operator

void putback(istream& data, const string& str);
bool is_stdin_empty();
void get_data_from_file(const string& fn, stringstream& data);
void get_data_from_stdin_or_file(const string& fn, stringstream& data, const bool try_stdin);
void print_sequence_header();
void print_sequence_footer();

// string chk(const string& id) { cout << id; return ""; }
